import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SequelizeModule } from '@nestjs/sequelize';
import models from './database/models/index';
import config from './config/db';
import { UserController } from './controllers/user/user.controller';
import { UserModule } from './modules/user/user.module';
import { AuthService } from './services/auth/auth.service';
import { AuthModule } from './modules/auth/auth.module';
import { LoginController } from './controllers/login/login.controller';
import { MenuController } from './controllers/menu/menu.controller';
import { MenuService } from './services/menu/menu.service';
import { MenuModule } from './modules/menu/menu.module';

// 导入所有模型
const allModels = [];
for (const key in models) {
  if (Object.prototype.hasOwnProperty.call(models, key)) {
    const element = models[key];
    allModels.push(element);
  }
}

const dbConfig = config.mysql;

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: dbConfig.host,
      port: dbConfig.port,
      username: dbConfig.username,
      password: dbConfig.password,
      database: dbConfig.database,
      logging: dbConfig.logging,
      // autoLoadModels: true,
      // synchronize: true,
      dialectOptions: dbConfig.dialectOptions,
      pool: dbConfig.pool,
      define: dbConfig.define,
      query: dbConfig.query,
      timezone: dbConfig.timezone,
      models: allModels,
    }),
    UserModule,
    AuthModule,
    MenuModule,
  ],
  controllers: [AppController, UserController, LoginController, MenuController],
  providers: [AppService],
})
export class AppModule {}
