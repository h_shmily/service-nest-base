import { Body, Controller, Post, UseGuards, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RbacGuard } from '../../guards/rbac.guard';
import { AddMenuDTO } from '../../dto/menu.dto';
import { ValidationPipe } from '../../pipe/validation.pipe';
import { MenuService } from '../../services/menu/menu.service';

// 拦截器与守卫均可用来做权限判断，此处建议采用守卫模式做权限判断
// @UseGuards(new RbacGuard(1))
// @UseInterceptors(new RbacInterceptor(1))

@Controller('menu')
export class MenuController {
  constructor(private readonly menuService: MenuService) {}
  @UseGuards(new RbacGuard(1)) // 权限判断
  @UseGuards(AuthGuard('jwt')) // 登陆鉴权
  @UsePipes(new ValidationPipe()) // 字段必传校验
  @Post('add')
  async addMenu(@Body() body: AddMenuDTO): Promise<any | undefined> {
    return await this.menuService.add(body);
  }
}
