import { IsNotEmpty } from 'class-validator';

export class AddMenuDTO {
  @IsNotEmpty({ message: '标题不能为空' })
  readonly title: string;
  @IsNotEmpty({ message: '副标题不能为空' })
  readonly subtitle: string;
  @IsNotEmpty({ message: '菜单名称不能为空' })
  readonly menu_name: string;
  @IsNotEmpty({ message: '路径不能为空' })
  readonly path: string;
}

export class UpdateMenuDTO {
  //
}
