/*
 * @Description: 这一步非必须，根据项目的需求来决定是否需要本地策略
 * @Version: 2.9.0
 * @Autor: hu-ai
 * @Date: 2021-08-19 14:41:03
 * @LastEditors: hu-ai
 * @LastEditTime: 2021-08-19 14:42:03
 */
import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    const user = await this.authService.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
