import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { encryptPassword } from '../../utils/cryptogram';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}
  // JWT验证 - Step 2: 校验用户信息
  async validateUser(
    account: string,
    password: string,
  ): Promise<any | undefined> {
    console.log('JWT验证 - Step 2: 校验用户信息');
    const user = await this.userService.isUser(account);
    if (user) {
      const hashedPassword = user.password;
      const salt = user.salt;
      const hashPassword = encryptPassword(password, salt);
      if (hashPassword === hashedPassword) {
        // 密码正确
        return {
          code: 1,
          user,
        };
      } else {
        // 密码错误
        return {
          code: 2,
          user: null,
        };
      }
    }
    // 查无此人
    return {
      code: 3,
      user: null,
    };
  }
  // JWT验证 - Step 3: 处理 jwt 签证
  async certificate(user: any) {
    // console.log(user);
    const payload = {
      account: user.account,
      sub: user.id,
      role: user.role,
    };
    console.log('JWT验证 - Step 3: 处理 jwt 签证', JSON.stringify(payload));
    try {
      const token = this.jwtService.sign(payload);
      return {
        code: 200,
        data: {
          token,
        },
        msg: `登录成功`,
      };
    } catch (error) {
      return {
        code: 600,
        msg: `账号或密码错误`,
      };
    }
  }
}
