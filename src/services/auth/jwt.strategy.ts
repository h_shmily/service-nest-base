/*
 * @Description: jwt.strategy.ts 用于编写 JWT 的验证策略
 * @Version: 2.9.0
 * @Autor: hu-ai
 * @Date: 2021-08-19 14:25:15
 * @LastEditors: hu-ai
 * @LastEditTime: 2022-04-09 10:03:30
 */
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstants } from './constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  // JWT验证 - Step 4: 被守卫调用
  async validate(payload: any) {
    console.log(payload);
    console.log(`JWT验证 - Step 4: 被守卫调用 ${JSON.stringify(payload)}`);
    return {
      account: payload.account,
      role: payload.role,
    };
  }
}
