import { Inject, Injectable } from '@nestjs/common';
import {
  Menu,
  menuAttributes,
  menuProvidersName,
} from '../../database/models/menu.entity';

@Injectable()
export class MenuService {
  constructor(
    @Inject(menuProvidersName)
    private readonly menusRepository: typeof Menu,
  ) {}
  // 新增菜单
  async add(requestBody: any) {
    try {
      const menuRes = await this.menusRepository.create(requestBody);
      return {
        code: 200,
        data: menuRes,
        msg: 'Success',
      };
    } catch (error) {
      return {
        code: 503,
        msg: `Service error: ${error}`,
      };
    }
  }
}
