/*
 * @Description: 用户表数据库模型
 * @Version: 2.9.0
 * @Autor: hu-ai
 * @Date: 2021-08-31 13:42:06
 * @LastEditors: hu-ai
 * @LastEditTime: 2022-04-08 17:21:53
 */
import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Length,
  DataType,
  CreatedAt,
  UpdatedAt,
  DeletedAt,
  AutoIncrement,
  Unique,
} from 'sequelize-typescript';

interface userAttributes {
  id?: number;
  account: string;
  name: string;
  avatar: string;
  password: string;
  sex?: number;
  email?: string;
  phone: string;
  status?: number;
  role?: string;
  area?: string;
  birthday?: Date;
  introduce?: string;
  salt: string;
  createdAt: Date;
}

@Table({
  tableName: 't_user',
  timestamps: false,
  comment: '113123213',
})
//@Column({ defaultValue: 设置默认值,  })
export class User
  extends Model<userAttributes, userAttributes>
  implements userAttributes
{
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Length({
    min: 4,
    max: 24,
    msg: `长度为4到24`,
  })
  @Column
  account: string;

  @Column
  name: string;

  @Column
  avatar: string;

  @Column
  password: string;

  @Column
  salt: string;

  @Column
  sex: number;

  @Column
  email: string;

  @Column
  phone: string;

  @Column
  status: number;

  @Column
  role: string;

  @Column
  area: string;

  @Column({ type: DataType.DATE })
  birthday: Date;

  @Column
  introduce: string;

  @CreatedAt
  @Column({ type: DataType.DATE })
  createdAt: Date;
}
export const userProvidersName = 'UsersRepository';
export const usersProviders = [
  {
    provide: userProvidersName,
    useValue: User,
  },
];
