import {
  Table,
  Model,
  PrimaryKey,
  Column,
  Length,
  DataType,
  CreatedAt,
  UpdatedAt,
  DeletedAt,
  AutoIncrement,
  Unique,
} from 'sequelize-typescript';

export interface menuAttributes {
  id?: number;
  title: string;
  subtitle: string;
  menu_name: string;
  path: string;
  target?: string;
  icon?: string;
  auth: string;
  menu_side?: string;
  divider?: number;
  createdAt: Date;
  UpdatedAt: Date;
}

@Table({
  tableName: 't_menu',
  timestamps: false,
  comment: '113123213',
})
//@Column({ defaultValue: 设置默认值,  })
export class Menu
  extends Model<menuAttributes, menuAttributes>
  implements menuAttributes
{
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  title: string;

  @Column
  subtitle: string;

  @Column
  menu_name: string;

  @Column
  path: string;

  @Column
  target?: string;

  @Column
  icon?: string;

  @Column
  auth: string;

  @Column
  menu_side?: string;

  @Column
  divider?: number;

  @CreatedAt
  @Column({ type: DataType.DATE })
  createdAt: Date;

  @UpdatedAt
  @Column({ type: DataType.DATE })
  UpdatedAt: Date;
}
export const menuProvidersName = 'MenuRepository';
export const menusProviders = [
  {
    provide: menuProvidersName,
    useValue: Menu,
  },
];
