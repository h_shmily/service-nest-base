import { User } from './user.entity';
import { Menu } from './menu.entity';

export default {
  User,
  Menu,
};
