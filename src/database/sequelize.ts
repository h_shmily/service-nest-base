import { Sequelize } from 'sequelize-typescript';
import dbConf from '../config/db';

const dbConfig = dbConf.mysql;

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.username,
  dbConfig.password || null,
  {
    host: dbConfig.host,
    dialect: 'mysql',
    logging: dbConfig.logging,
    dialectOptions: dbConfig.dialectOptions,
    pool: dbConfig.pool,
    define: dbConfig.define,
    query: dbConfig.query,
    timezone: dbConfig.timezone,
  },
);

sequelize
  .authenticate()
  .then(() => {
    console.log('数据库连接成功');
  })
  .catch((err) => {
    // 数据库连接失败时打印输出
    console.error(err);
    throw err;
  });

export default sequelize;
