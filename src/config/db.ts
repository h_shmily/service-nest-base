const productConfig = {
  mysql: {
    dialect: 'mysql',
    port: 3306,
    host: '127.0.0.1',
    username: 'root',
    password: 'ch890818',
    database: 'sensitive', // 库名
    connectionLimit: 10, // 连接限制
    logging: false,
    timezone: '+08:00',
    dialectOptions: {
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
      supportBigNumbers: true,
      bigNumberStrings: true,
    },
    pool: {
      max: 5, // 连接池中最大连接数量
      min: 0, // 连接池中最小连接数量
      acquire: 30000,
      idle: 10000, // 如果一个线程 10 秒钟内没有被使用过的话，那么就释放线程
    },
    define: {
      timestamps: true, // 是否自动创建时间字段， 默认会自动创建createdAt、updatedAt
      paranoid: true, // 是否自动创建deletedAt字段
      createdAt: 'createTime', // 重命名字段
      updatedAt: 'updateTime',
      deletedAt: 'deleteTime',
      underscored: true, // 开启下划线命名方式，默认是驼峰命名
      freezeTableName: true, // 禁止修改表名
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
    },
    query: {
      raw: true, // 设置为 true，即可返回源数据
    },
  },
};

const localConfig = {
  mysql: {
    dialect: 'mysql',
    port: 3306,
    host: '127.0.0.1',
    username: 'root',
    password: 'ch890818',
    database: 'sensitive', // 库名
    connectionLimit: 10, // 连接限制
    logging: false,
    timezone: '+08:00',
    dialectOptions: {
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
      supportBigNumbers: true,
      bigNumberStrings: true,
    },
    pool: {
      max: 5, // 连接池中最大连接数量
      min: 0, // 连接池中最小连接数量
      acquire: 30000,
      idle: 10000, // 如果一个线程 10 秒钟内没有被使用过的话，那么就释放线程
    },
    define: {
      timestamps: true, // 是否自动创建时间字段， 默认会自动创建createdAt、updatedAt
      paranoid: true, // 是否自动创建deletedAt字段
      createdAt: 'createTime', // 重命名字段
      updatedAt: 'updateTime',
      deletedAt: 'deleteTime',
      underscored: true, // 开启下划线命名方式，默认是驼峰命名
      freezeTableName: true, // 禁止修改表名
      charset: 'utf8mb4',
      collate: 'utf8mb4_unicode_ci',
    },
    query: {
      raw: true, // 设置为 true，即可返回源数据
    },
  },
};

const config = process.env.NODE_ENV ? productConfig : localConfig;

export default config;
