import { Module } from '@nestjs/common';
import { usersProviders } from '../../database/models/user.entity';
import { UserService } from '../..//services/user/user.service';

@Module({
  imports: [],
  providers: [UserService, ...usersProviders],
  exports: [UserService],
})
export class UserModule {}
