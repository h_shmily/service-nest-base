import { Module } from '@nestjs/common';
import { menusProviders } from '../../database/models/menu.entity';
import { MenuService } from '../../services/menu/menu.service';

@Module({
  imports: [],
  providers: [MenuService, ...menusProviders],
  exports: [MenuService],
})
export class MenuModule {}
