import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import { AppModule } from './app.module';
import { AnyExceptionFilter } from './filter/any-exception.filter';
import { HttpExceptionFilter } from './filter/http-exception.filter';
import { TransformInterceptor } from './interceptor/transform.interceptor';
import { logger } from './middleware/logger.middleware';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // 处理请求参数
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // 利用中间件，监听所有的请求路由，并打印日志
  app.use(logger);

  // 拦截器
  app.useGlobalInterceptors(new TransformInterceptor());

  // 过滤处理 HTTP 异常
  app.useGlobalFilters(new AnyExceptionFilter());
  app.useGlobalFilters(new HttpExceptionFilter());

  // app.setGlobalPrefix();

  await app.listen(12300);
}
bootstrap();
